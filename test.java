

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;



class Result {

    /*
     * Complete the 'processLogs' function below.
     *
     * The function is expected to return a STRING_ARRAY.
     * The function accepts following parameters:
     *  1. STRING_ARRAY logs
     *  2. INTEGER maxSpan
     */

    public static List<String> processLogs(List<String> logs, int maxSpan) {
        String signInAction = "sign-in";
        String signOutAction = "sign-out"; 
        int logLength = logs.size();
        HashMap <String, ArrayList<Integer>> map = new HashMap<String, ArrayList<Integer>>(); 
        for(int i= 0; i < logs.size(); i++) {
            String log = logs.get(i);
            String [] logContent = log.split(" ");
            String key = String.valueOf(logContent[0]);
            int value = Integer.parseInt(logContent[1]);
            String action = logContent[2];
            if (map.get(key) == null) {
                ArrayList<Integer> list = new ArrayList<Integer>();
                list.add(null);
                list.add(null);
                map.put(key, list);
            }
            if(action.equals(signInAction)) {
                ArrayList<Integer> list = map.get(key);
                list.set(0, value);
                map.put(key, list);
            }
            else if(action.equals(signOutAction)) {
                ArrayList<Integer> list = map.get(key);
                list.set(1, value);
                map.put(key, list);
            }   
        }
        
        ArrayList<String> list = new ArrayList<String>();
        for (Map.Entry<String, ArrayList<Integer>> entry : map.entrySet()) {
            ArrayList<Integer> results = entry.getValue();
            if(results.get(0) != null &&
               results.get(1) != null &&
               Math.abs((results.get(0) - results.get(1))) <= maxSpan) 
            {
                list.add(entry.getKey());
            }
        }
        return list;

    }

}
